import java.net.*;
import java.io.*;
import java.util.regex.*;

public class lrodgershttp {

    public static void main(String[] args) {

        try {
            //This sets the website to the LDS Newsroom site
            URL url = new URL("https://newsroom.churchofjesuschrist.org/");

            //This opens the connection to the website
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            //Starts the buffered reader
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));

            //Setting a spot to hold the code coming in from the reader
            StringBuilder newString = new StringBuilder();


            //This brings all the code in and saves it in newString
            String line;
            while ((line= reader.readLine()) != null) {
                newString.append(line + "\n");
            }

            //Changes all the code into a string
            String codeString = newString.toString();

            //Looks for a certain pattern in the HTML code where the headlines are found.
            Pattern pattern = Pattern.compile("-title\">(.+)</s");
            Matcher matcher = pattern.matcher(codeString);

            System.out.println("\nThese are currently the top headlines on the LDS Newsroom website:\n");

            //Outputs all parts of the string where the pattern matches
            //and puts out all the headlines
            while (matcher.find())
                {
                    String headlines = matcher.group(1);
                    System.out.format("-%s\n", headlines);
                }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}